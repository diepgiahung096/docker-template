FROM ubuntu

LABEL author="hung.diep@alpaca.vn"

WORKDIR /app

COPY render-template .

RUN apt-get update \
    && apt-get install python3-pip vim -y \
    && pip install jinja2