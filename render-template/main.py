#!/usr/bin/env python3
from jinja2 import Environment, FileSystemLoader
import os

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)
template = env.get_template('output.tmpl.j2')

Commit1 = os.environ.get('S1')
Commit2 = os.environ.get('S2')
Commit3 = os.environ.get('S3')

output = template.render(S1=Commit1, S2=Commit2, S3=Commit3)

print(output)
